/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./development/components/js/connect.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./development/components/js/connect.js":
/*!**********************************************!*\
  !*** ./development/components/js/connect.js ***!
  \**********************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _modules_moveScrollUP__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./modules/moveScrollUP */ "./development/components/js/modules/moveScrollUP.js");
/* harmony import */ var _modules_moveScrollUP__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_modules_moveScrollUP__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _modules_openMyBurger__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./modules/openMyBurger */ "./development/components/js/modules/openMyBurger.js");
/* harmony import */ var _modules_openMyBurger__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_modules_openMyBurger__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _modules_AddMyWorker__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./modules/AddMyWorker */ "./development/components/js/modules/AddMyWorker.js");
/* harmony import */ var _modules_AddMyWorker__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_modules_AddMyWorker__WEBPACK_IMPORTED_MODULE_2__);
// ============================
//    Name: index.js
// ============================




var start = function start() {
  console.log('DOM:', 'DOMContentLoaded', true);
  new _modules_moveScrollUP__WEBPACK_IMPORTED_MODULE_0___default.a({
    selector: '.js__moveScrollUP',
    speed: 8
  }).run();
  new _modules_openMyBurger__WEBPACK_IMPORTED_MODULE_1___default.a({
    burger: '.js__navHamburger',
    navbar: '.js__navHamburgerOpener'
  }).run();
  new _modules_AddMyWorker__WEBPACK_IMPORTED_MODULE_2___default.a({
    selector: '.js__myTaskList',
    addbutton: '.js__addNewTask'
  }).run();
};

if (typeof window !== 'undefined' && window && window.addEventListener) {
  document.addEventListener('DOMContentLoaded', start(), false);
}

/***/ }),

/***/ "./development/components/js/modules/AddMyWorker.js":
/*!**********************************************************!*\
  !*** ./development/components/js/modules/AddMyWorker.js ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

module.exports =
/*#__PURE__*/
function () {
  function AddOpenMyBurger(init) {
    _classCallCheck(this, AddOpenMyBurger);

    this.selector = document.querySelector(init.selector);
    this.addbutton = document.querySelector(init.addbutton);
    this.errorText = '💀 Ouch, database error...';
    this.modalInput = document.querySelector('#modal__input_id');
    this.openModal = document.querySelector('.js__openMyModal');
    this.createModal = document.querySelector('.js__myModalCreate');
    this.cardEmpty = document.querySelector('.card__empty');
    this.modalCount = 0;
    this.inputTextLeght = 3;
  }

  _createClass(AddOpenMyBurger, [{
    key: "run",
    value: function run() {
      var _this = this;

      if (this.selector) {
        this.constructor.info();
        var pastHTML = {
          card: function card(titleName, pastCount) {
            return "\n\t\t\t\t\t\t\t\t\t<div class=\"card__head\">\n\t\t\t\t\t\t\t\t\t<h4 class=\"card__title\">".concat(titleName, "</h4>\n\t\t\t\t\t\t\t\t\t<div class=\"card__nav\">\n\t\t\t\t\t\t\t\t\t<div class=\"card__group\">\n\t\t\t\t\t\t\t\t\t<div class=\"card__item card__item-status\"><span data-worker-count>").concat(pastCount, "</span><p>\u0432\u0430\u043A\u0430\u043D\u0441\u0438\u0438</p></div>\n\t\t\t\t\t\t\t\t\t<div class=\"card__item card__item-add active\" data-project>\u0414\u043E\u0431\u0430\u0432\u0438\u0442\u044C \u0432\u0430\u043A\u0430\u043D\u0441\u0438\u044E</div>\n\t\t\t\t\t\t\t\t\t<div class=\"card__item card__item-close\" data-project>\n\t\t\t\t\t\t\t\t\t<span><svg role=\"picture-svg\" class=\"glyphs__checkbox\"><use xlink:href=\"#id-glyphs-checkbox\"></use></svg></span>\n\t\t\t\t\t\t\t\t\t<p class=\"card__text\">\u041F\u0440\u043E\u0435\u043A\u0442 \u0437\u0430\u043A\u0440\u044B\u0442, \u0441\u043E\u0442\u0440\u0443\u0434\u043D\u0438\u043A\u0438 \u043D\u0430\u043D\u044F\u0442\u044B</p>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t<div class=\"card__group\">\n\t\t\t\t\t\t\t\t\t<div class=\"card__item card__item-exit active\" data-reopen>\u0417\u0430\u043A\u0440\u044B\u0442\u044C \u043F\u0440\u043E\u0435\u043A\u0442</div>\n\t\t\t\t\t\t\t\t\t<div class=\"card__item card__item-reopen\" data-reopen>\u041E\u0442\u043A\u0440\u044B\u0442\u044C \u043F\u0440\u043E\u0435\u043A\u0442</div>\n\t\t\t\t\t\t\t\t\t<div class=\"card__item\">\n\t\t\t\t\t\t\t\t\t<p class=\"card__text card__text-kill active\" data-status>\u0423\u0434\u0430\u043B\u0438\u0442\u044C</p>\n\t\t\t\t\t\t\t\t\t<div class=\"card__item-remove\" data-status>\n\t\t\t\t\t\t\t\t\t<span data-status-yes><svg role=\"picture-svg\" class=\"glyphs__checkbox\"><use xlink:href=\"#id-glyphs-checkbox\"></use></svg></span>\n\t\t\t\t\t\t\t\t\t<span data-status-nope><svg role=\"picture-svg\" class=\"glyphs__closemodal\"><use xlink:href=\"#id-glyphs-closemodal\"></use></svg></span>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t");
          },
          cardlist: function cardlist(titleName) {
            return "\n\t\t\t\t\t\t\t\t\t<div class=\"card-list__head\">\n\t\t\t\t\t\t\t\t\t<h4 class=\"card-list__title active\" data-list-title>\u041F\u0443\u0441\u0442\u043E</h4>\n\t\t\t\t\t\t\t\t\t<input class=\"card-list__title card-list__title-edit\" type=\"text\" name=\"text\" autocomplete=\"off\" maxlength=\"120\">\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t<div class=\"card-list__nav\">\n\t\t\t\t\t\t\t\t\t<div class=\"card-list__group\">\n\t\t\t\t\t\t\t\t\t<div class=\"card-list__item card-list__item-search active\" data-list-project>\n\t\t\t\t\t\t\t\t\t<span><svg role=\"picture-svg\" class=\"glyphs__search\"><use xlink:href=\"#id-glyphs-search\"></use></svg></span>\n\t\t\t\t\t\t\t\t\t<p class=\"card-list__text\">\u0412\u0430\u043A\u0430\u043D\u0441\u0438\u044F \u043E\u0442\u043A\u0440\u044B\u0442\u0430, \u0438\u0434\u0435\u0442 \u043F\u043E\u0434\u0431\u043E\u0440 \u043A\u0430\u043D\u0434\u0438\u0434\u0430\u0442\u043E\u0432</p>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t<div class=\"card-list__item card-list__item-search\" data-list-project>\n\t\t\t\t\t\t\t\t\t<span><svg role=\"picture-svg\" class=\"glyphs__checkbox\"><use xlink:href=\"#id-glyphs-checkbox\"></use></svg></span>\n\t\t\t\t\t\t\t\t\t<p class=\"card-list__text\">\u0412\u0430\u043A\u0430\u043D\u0441\u0438\u044F \u0437\u0430\u043A\u0440\u044B\u0442\u0430, \u0441\u043E\u0442\u0440\u0443\u0434\u043D\u0438\u043A \u043D\u0430\u043D\u044F\u0442</p>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t<div class=\"card-list__group js__toggleListReopen\">\n\t\t\t\t\t\t\t\t\t<div class=\"card-list__item card-list__item-exit active\" data-list-reopen>\u0417\u0430\u043A\u0440\u044B\u0442\u044C \u0432\u0430\u043A\u0430\u043D\u0441\u0438\u044E</div>\n\t\t\t\t\t\t\t\t\t<div class=\"card-list__item card-list__item-reopen\" data-list-reopen>\u041E\u0442\u043A\u0440\u044B\u0442\u044C \u0432\u0430\u043A\u0430\u043D\u0441\u0438\u044E</div>\n\t\t\t\t\t\t\t\t\t<div class=\"card-list__item js__toggleListKill\">\n\t\t\t\t\t\t\t\t\t<p class=\"card-list__text card-list__text-kill active\" data-list-status>\u0423\u0434\u0430\u043B\u0438\u0442\u044C</p>\n\t\t\t\t\t\t\t\t\t<div class=\"card-list__item-remove\" data-list-status>\n\t\t\t\t\t\t\t\t\t<span data-list-status-yes><svg role=\"picture-svg\" class=\"glyphs__checkbox\"><use xlink:href=\"#id-glyphs-checkbox\"></use></svg></span>\n\t\t\t\t\t\t\t\t\t<span data-list-status-nope><svg role=\"picture-svg\" class=\"glyphs__closemodal\"><use xlink:href=\"#id-glyphs-closemodal\"></use></svg></span>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t";
          }
        };

        var observerCardCount = function observerCardCount() {
          if (_this.selector.querySelector('.js__toggleStatus') <= 0) {
            _this.cardEmpty.classList.add('active');
          } else _this.cardEmpty.classList.remove('active');
        };

        var observerCard = new MutationObserver(observerCardCount);
        observerCard.observe(this.selector, {
          childList: true
        }, false);

        var updateListCount = function updateListCount(event) {
          var list = _toConsumableArray(document.querySelectorAll('.js__toggleStatus'));

          list.forEach(function (f) {
            var count = f.childElementCount - 1;
            var item = f.querySelector('[data-worker-count]');
            item.textContent = count || 0;
          });
        };

        updateListCount();

        var openModalState = function openModalState(event) {
          var _target = event.target;

          var animaEnd = function animaEnd() {
            _this.openModal.classList.remove('anima-opacity-hide', 'active');

            _this.modalInput.classList.remove('error');

            _this.modalInput.value = '';
            _this.modalInput.nextElementSibling.textContent = '';
          };

          if (_target.matches('.js__openMyModal.active') || _target.matches('.modal__close')) {
            _this.openModal.classList.add('anima-opacity-hide');

            _this.openModal.addEventListener('animationend', animaEnd, {
              once: true
            }, false);
          }
        };

        var addbuttonOpacity = function addbuttonOpacity() {
          var animaEnd = function animaEnd() {
            return _this.openModal.classList.remove('anima-opacity-show');
          };

          _this.openModal.classList.add('active');

          _this.openModal.classList.add('anima-opacity-show');

          _this.openModal.addEventListener('animationend', animaEnd, {
            once: true
          }, false);
        };

        var createNewTask = function createNewTask() {
          var pastInput = _this.modalInput.value || 'Пусто';
          var pastCount = _this.modalCount;

          var animaEnd = function animaEnd() {
            return createDiv.classList.remove('anima-add-me');
          };

          var createDiv = document.createElement('DIV');
          createDiv.classList.add('card__box', 'js__toggleStatus', 'anima-add-me');
          createDiv.innerHTML = pastHTML.card(pastInput, pastCount);

          _this.selector.appendChild(createDiv);

          _this.openModal.classList.toggle('active');

          _this.modalInput.value = '';
          createDiv.addEventListener('animationend', animaEnd, {
            once: true
          }, false);
        };

        var createNewTaskInputValidat = function createNewTaskInputValidat() {
          if (_this.modalInput.value.length >= _this.inputTextLeght) {
            _this.modalInput.classList.remove('error');

            _this.modalInput.nextElementSibling.textContent = '';
          }
        };

        var createNewTaskValidat = function createNewTaskValidat() {
          if (_this.modalInput.value.length <= _this.inputTextLeght) {
            _this.modalInput.classList.add('error');

            _this.modalInput.nextElementSibling.textContent = "\u0412\u0432\u0435\u0434\u0435\u043D\u043D\u044B\u0435 \u0434\u0430\u043D\u043D\u044B\u0435 \u043C\u0435\u043D\u044C\u0448\u0435 ".concat(_this.inputTextLeght, " \u0441\u0438\u043C\u0432\u043E\u043B\u043E\u0432");
            return;
          }

          createNewTask();
        };

        var createNewList = function createNewList(event) {
          var myTarget = event.target.closest('.js__toggleStatus');

          if (myTarget && event.target.matches('[data-project]')) {
            var count = myTarget.childElementCount || 0;
            var createDiv = document.createElement('DIV');
            createDiv.classList.add('card-list__box');
            createDiv.innerHTML += pastHTML.cardlist('Нажми меня!');
            event.target.closest('.card__box').appendChild(createDiv);
            myTarget.querySelector('[data-worker-count]').textContent = count;
          }
        };

        var toggleActiveClass = function toggleActiveClass(event) {
          var myTarget = event.target.closest('.js__toggleStatus');
          var myListTargetReopen = event.target.closest('.js__toggleListReopen');
          var myListTargetKill = event.target.closest('.js__toggleListKill');

          if (myTarget && event.target.matches('[data-list-title]')) {
            var _target = event.target;
            var inputText = event.target.nextElementSibling;
            inputText.value = _target.textContent;

            _target.classList.toggle('active');

            _target.nextElementSibling.focus();

            _target.nextElementSibling.select();

            var myFocusFunc = function myFocusFunc(ev) {
              if (ev.key === 'Enter') inputText.blur();
            };

            var myBlurOut = function myBlurOut() {
              if (event.defaultPrevented) return;
              _target.textContent = inputText.value || 'Пусто';
              setTimeout(function () {
                return _target.classList.toggle('active');
              }, 250);
              inputText.removeEventListener('keypress', myFocusFunc);
            };

            inputText.addEventListener('blur', myBlurOut, {
              once: true
            }, false);
            inputText.addEventListener('keypress', myFocusFunc, false);
          }

          if (myTarget && (event.target.matches('[data-status]') || event.target.matches('[data-status-nope]'))) {
            _toConsumableArray(myTarget.querySelectorAll('[data-status]')).forEach(function (f) {
              return f.classList.toggle('active');
            });
          }

          if (myTarget && event.target.matches('[data-status-yes]')) {
            var myCard = event.target.closest('.card__box');

            var animaEnd = function animaEnd() {
              return myCard.remove();
            };

            myCard.style.maxHeight = "".concat(myCard.offsetHeight, "px");
            myCard.classList.add('anima-remove-me');
            myCard.addEventListener('animationend', animaEnd, {
              once: true
            }, false);
          }

          if (myTarget && event.target.matches('[data-reopen]')) {
            _toConsumableArray(myTarget.querySelectorAll('[data-reopen]')).forEach(function (f) {
              return f.classList.toggle('active');
            });

            _toConsumableArray(myTarget.querySelectorAll('[data-project]')).forEach(function (f) {
              return f.classList.toggle('active');
            });
          }

          if (myListTargetKill && (event.target.matches('[data-list-status]') || event.target.matches('[data-list-status-nope]'))) {
            _toConsumableArray(myListTargetKill.querySelectorAll('[data-list-status]')).forEach(function (f) {
              return f.classList.toggle('active');
            });
          }

          if (myListTargetKill && event.target.matches('[data-list-status-yes]')) {
            var listCount = myTarget.childElementCount - 2 || 0;
            var myCardList = event.target.closest('.card-list__box');
            myCardList.classList.add('anima-remove-me');
            myCardList.addEventListener('animationend', function () {
              return myCardList.remove();
            }, {
              once: true
            }, false);
            myTarget.querySelector('[data-worker-count]').textContent = listCount;
          }

          if (myListTargetReopen && event.target.matches('[data-list-reopen]')) {
            var _myCardList = event.target.closest('.card-list__box');

            _toConsumableArray(myListTargetReopen.querySelectorAll('[data-list-reopen]')).forEach(function (f) {
              return f.classList.toggle('active');
            });

            _toConsumableArray(myListTargetReopen.parentElement.querySelectorAll('[data-list-project]')).forEach(function (f) {
              return f.classList.toggle('active');
            });

            _myCardList.querySelector('[data-list-title]').classList.toggle('pointerevents');
          }
        };

        this.addbutton.addEventListener('click', addbuttonOpacity, false);
        this.openModal.addEventListener('click', openModalState, false);
        this.modalInput.addEventListener('input', createNewTaskInputValidat, false);
        this.createModal.addEventListener('click', createNewTaskValidat, false);
        this.selector.addEventListener('click', createNewList, false);
        this.selector.addEventListener('click', toggleActiveClass, false);
      }
    }
  }], [{
    key: "info",
    value: function info() {
      console.log('MODULE:', this.name, true);
    }
  }]);

  return AddOpenMyBurger;
}();

/***/ }),

/***/ "./development/components/js/modules/moveScrollUP.js":
/*!***********************************************************!*\
  !*** ./development/components/js/modules/moveScrollUP.js ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

module.exports =
/*#__PURE__*/
function () {
  function AddMoveScrollUP(init) {
    _classCallCheck(this, AddMoveScrollUP);

    this.selector = init.selector;
    this.count = Number(Math.abs(init.speed)) || 8;
    this.speed = this.count <= 20 ? this.count : 8;
    this.myPos = window.pageYOffset;
    this.getScroll = document.documentElement.scrollTop;
    this.speedScroll = this.speed;
  }

  _createClass(AddMoveScrollUP, [{
    key: "run",
    value: function run() {
      var _this = this;

      var selector = document.querySelector("".concat(this.selector));

      if (selector) {
        this.constructor.info();

        var clickedArrow = function clickedArrow() {
          _this.getScroll = document.documentElement.scrollTop;

          if (_this.getScroll >= 1) {
            window.requestAnimationFrame(clickedArrow);
            window.scrollTo(0, _this.getScroll - _this.getScroll / _this.speedScroll);
          }
        };

        var scrolledDown = function scrolledDown() {
          _this.myPos = window.pageYOffset;
          _this.myPos >= 100 ? selector.classList.add('open') : selector.classList.remove('open');
        };

        selector.addEventListener('click', clickedArrow);
        window.addEventListener('scroll', scrolledDown);
      }
    }
  }], [{
    key: "info",
    value: function info() {
      console.log('MODULE:', this.name, true);
    }
  }]);

  return AddMoveScrollUP;
}();

/***/ }),

/***/ "./development/components/js/modules/openMyBurger.js":
/*!***********************************************************!*\
  !*** ./development/components/js/modules/openMyBurger.js ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

module.exports =
/*#__PURE__*/
function () {
  function AddOpenMyBurger(init) {
    _classCallCheck(this, AddOpenMyBurger);

    this.selector = init.burger;
    this.navbar = init.navbar;
    this.errorText = '💀 Ouch, database error...';
  }

  _createClass(AddOpenMyBurger, [{
    key: "run",
    value: function run() {
      var _this = this;

      var selector = document.querySelector("".concat(this.selector));

      if (selector) {
        this.constructor.info();

        var openBurger = function openBurger() {
          var nav = document.querySelector("".concat(_this.navbar));
          nav.querySelector('DIV').classList.toggle('open');
          selector.classList.toggle('open');
        };

        selector.addEventListener('click', openBurger);
      }
    }
  }], [{
    key: "info",
    value: function info() {
      console.log('MODULE:', this.name, true);
    }
  }]);

  return AddOpenMyBurger;
}();

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29ubmVjdC5qcyIsInNvdXJjZXMiOlsid2VicGFjazovLy93ZWJwYWNrL2Jvb3RzdHJhcCIsIndlYnBhY2s6Ly8vLi9kZXZlbG9wbWVudC9jb21wb25lbnRzL2pzL2Nvbm5lY3QuanMiLCJ3ZWJwYWNrOi8vLy4vZGV2ZWxvcG1lbnQvY29tcG9uZW50cy9qcy9tb2R1bGVzL0FkZE15V29ya2VyLmpzIiwid2VicGFjazovLy8uL2RldmVsb3BtZW50L2NvbXBvbmVudHMvanMvbW9kdWxlcy9tb3ZlU2Nyb2xsVVAuanMiLCJ3ZWJwYWNrOi8vLy4vZGV2ZWxvcG1lbnQvY29tcG9uZW50cy9qcy9tb2R1bGVzL29wZW5NeUJ1cmdlci5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyIgXHQvLyBUaGUgbW9kdWxlIGNhY2hlXG4gXHR2YXIgaW5zdGFsbGVkTW9kdWxlcyA9IHt9O1xuXG4gXHQvLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuIFx0ZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXG4gXHRcdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuIFx0XHRpZihpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSkge1xuIFx0XHRcdHJldHVybiBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXS5leHBvcnRzO1xuIFx0XHR9XG4gXHRcdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG4gXHRcdHZhciBtb2R1bGUgPSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSA9IHtcbiBcdFx0XHRpOiBtb2R1bGVJZCxcbiBcdFx0XHRsOiBmYWxzZSxcbiBcdFx0XHRleHBvcnRzOiB7fVxuIFx0XHR9O1xuXG4gXHRcdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuIFx0XHRtb2R1bGVzW21vZHVsZUlkXS5jYWxsKG1vZHVsZS5leHBvcnRzLCBtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcblxuIFx0XHQvLyBGbGFnIHRoZSBtb2R1bGUgYXMgbG9hZGVkXG4gXHRcdG1vZHVsZS5sID0gdHJ1ZTtcblxuIFx0XHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuIFx0XHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG4gXHR9XG5cblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGVzIG9iamVjdCAoX193ZWJwYWNrX21vZHVsZXNfXylcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubSA9IG1vZHVsZXM7XG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlIGNhY2hlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmMgPSBpbnN0YWxsZWRNb2R1bGVzO1xuXG4gXHQvLyBkZWZpbmUgZ2V0dGVyIGZ1bmN0aW9uIGZvciBoYXJtb255IGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uZCA9IGZ1bmN0aW9uKGV4cG9ydHMsIG5hbWUsIGdldHRlcikge1xuIFx0XHRpZighX193ZWJwYWNrX3JlcXVpcmVfXy5vKGV4cG9ydHMsIG5hbWUpKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIG5hbWUsIHsgZW51bWVyYWJsZTogdHJ1ZSwgZ2V0OiBnZXR0ZXIgfSk7XG4gXHRcdH1cbiBcdH07XG5cbiBcdC8vIGRlZmluZSBfX2VzTW9kdWxlIG9uIGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uciA9IGZ1bmN0aW9uKGV4cG9ydHMpIHtcbiBcdFx0aWYodHlwZW9mIFN5bWJvbCAhPT0gJ3VuZGVmaW5lZCcgJiYgU3ltYm9sLnRvU3RyaW5nVGFnKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFN5bWJvbC50b1N0cmluZ1RhZywgeyB2YWx1ZTogJ01vZHVsZScgfSk7XG4gXHRcdH1cbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsICdfX2VzTW9kdWxlJywgeyB2YWx1ZTogdHJ1ZSB9KTtcbiBcdH07XG5cbiBcdC8vIGNyZWF0ZSBhIGZha2UgbmFtZXNwYWNlIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDE6IHZhbHVlIGlzIGEgbW9kdWxlIGlkLCByZXF1aXJlIGl0XG4gXHQvLyBtb2RlICYgMjogbWVyZ2UgYWxsIHByb3BlcnRpZXMgb2YgdmFsdWUgaW50byB0aGUgbnNcbiBcdC8vIG1vZGUgJiA0OiByZXR1cm4gdmFsdWUgd2hlbiBhbHJlYWR5IG5zIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDh8MTogYmVoYXZlIGxpa2UgcmVxdWlyZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy50ID0gZnVuY3Rpb24odmFsdWUsIG1vZGUpIHtcbiBcdFx0aWYobW9kZSAmIDEpIHZhbHVlID0gX193ZWJwYWNrX3JlcXVpcmVfXyh2YWx1ZSk7XG4gXHRcdGlmKG1vZGUgJiA4KSByZXR1cm4gdmFsdWU7XG4gXHRcdGlmKChtb2RlICYgNCkgJiYgdHlwZW9mIHZhbHVlID09PSAnb2JqZWN0JyAmJiB2YWx1ZSAmJiB2YWx1ZS5fX2VzTW9kdWxlKSByZXR1cm4gdmFsdWU7XG4gXHRcdHZhciBucyA9IE9iamVjdC5jcmVhdGUobnVsbCk7XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18ucihucyk7XG4gXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShucywgJ2RlZmF1bHQnLCB7IGVudW1lcmFibGU6IHRydWUsIHZhbHVlOiB2YWx1ZSB9KTtcbiBcdFx0aWYobW9kZSAmIDIgJiYgdHlwZW9mIHZhbHVlICE9ICdzdHJpbmcnKSBmb3IodmFyIGtleSBpbiB2YWx1ZSkgX193ZWJwYWNrX3JlcXVpcmVfXy5kKG5zLCBrZXksIGZ1bmN0aW9uKGtleSkgeyByZXR1cm4gdmFsdWVba2V5XTsgfS5iaW5kKG51bGwsIGtleSkpO1xuIFx0XHRyZXR1cm4gbnM7XG4gXHR9O1xuXG4gXHQvLyBnZXREZWZhdWx0RXhwb3J0IGZ1bmN0aW9uIGZvciBjb21wYXRpYmlsaXR5IHdpdGggbm9uLWhhcm1vbnkgbW9kdWxlc1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5uID0gZnVuY3Rpb24obW9kdWxlKSB7XG4gXHRcdHZhciBnZXR0ZXIgPSBtb2R1bGUgJiYgbW9kdWxlLl9fZXNNb2R1bGUgP1xuIFx0XHRcdGZ1bmN0aW9uIGdldERlZmF1bHQoKSB7IHJldHVybiBtb2R1bGVbJ2RlZmF1bHQnXTsgfSA6XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0TW9kdWxlRXhwb3J0cygpIHsgcmV0dXJuIG1vZHVsZTsgfTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kKGdldHRlciwgJ2EnLCBnZXR0ZXIpO1xuIFx0XHRyZXR1cm4gZ2V0dGVyO1xuIFx0fTtcblxuIFx0Ly8gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm8gPSBmdW5jdGlvbihvYmplY3QsIHByb3BlcnR5KSB7IHJldHVybiBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqZWN0LCBwcm9wZXJ0eSk7IH07XG5cbiBcdC8vIF9fd2VicGFja19wdWJsaWNfcGF0aF9fXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnAgPSBcIlwiO1xuXG5cbiBcdC8vIExvYWQgZW50cnkgbW9kdWxlIGFuZCByZXR1cm4gZXhwb3J0c1xuIFx0cmV0dXJuIF9fd2VicGFja19yZXF1aXJlX18oX193ZWJwYWNrX3JlcXVpcmVfXy5zID0gXCIuL2RldmVsb3BtZW50L2NvbXBvbmVudHMvanMvY29ubmVjdC5qc1wiKTtcbiIsIi8vID09PT09PT09PT09PT09PT09PT09PT09PT09PT1cclxuLy8gICAgTmFtZTogaW5kZXguanNcclxuLy8gPT09PT09PT09PT09PT09PT09PT09PT09PT09PVxyXG5cclxuaW1wb3J0IEFkZE1vdmVTY3JvbGxVUCBmcm9tICcuL21vZHVsZXMvbW92ZVNjcm9sbFVQJztcclxuaW1wb3J0IEFkZE9wZW5NeUJ1cmdlciBmcm9tICcuL21vZHVsZXMvb3Blbk15QnVyZ2VyJztcclxuaW1wb3J0IEFkZE15V29ya2VyIGZyb20gJy4vbW9kdWxlcy9BZGRNeVdvcmtlcic7XHJcblxyXG5jb25zdCBzdGFydCA9ICgpID0+IHtcclxuXHRjb25zb2xlLmxvZygnRE9NOicsICdET01Db250ZW50TG9hZGVkJywgdHJ1ZSk7XHJcblxyXG5cdG5ldyBBZGRNb3ZlU2Nyb2xsVVAoe1xyXG5cdFx0c2VsZWN0b3I6ICcuanNfX21vdmVTY3JvbGxVUCcsXHJcblx0XHRzcGVlZDogOFxyXG5cdH0pLnJ1bigpO1xyXG5cdFxyXG5cdG5ldyBBZGRPcGVuTXlCdXJnZXIoe1xyXG5cdFx0YnVyZ2VyOiAnLmpzX19uYXZIYW1idXJnZXInLFxyXG5cdFx0bmF2YmFyOiAnLmpzX19uYXZIYW1idXJnZXJPcGVuZXInXHJcblx0fSkucnVuKCk7XHJcblx0XHJcblx0bmV3IEFkZE15V29ya2VyKHtcclxuXHRcdHNlbGVjdG9yOiAnLmpzX19teVRhc2tMaXN0JyxcclxuXHRcdGFkZGJ1dHRvbjogJy5qc19fYWRkTmV3VGFzaydcclxuXHR9KS5ydW4oKTtcclxuXHRcclxufTtcclxuXHJcbmlmICh0eXBlb2Ygd2luZG93ICE9PSAndW5kZWZpbmVkJyAmJiB3aW5kb3cgJiYgd2luZG93LmFkZEV2ZW50TGlzdGVuZXIpIHtcclxuXHRkb2N1bWVudC5hZGRFdmVudExpc3RlbmVyKCdET01Db250ZW50TG9hZGVkJywgc3RhcnQoKSwgZmFsc2UpO1xyXG59XHJcbiAiLCJtb2R1bGUuZXhwb3J0cyA9IGNsYXNzIEFkZE9wZW5NeUJ1cmdlciB7XHJcblx0Y29uc3RydWN0b3IoaW5pdCkge1xyXG5cdFx0dGhpcy5zZWxlY3RvciA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoaW5pdC5zZWxlY3Rvcik7XHJcblx0XHR0aGlzLmFkZGJ1dHRvbiA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoaW5pdC5hZGRidXR0b24pO1xyXG5cdFx0dGhpcy5lcnJvclRleHQgPSAn8J+SgCBPdWNoLCBkYXRhYmFzZSBlcnJvci4uLic7XHJcblxyXG5cdFx0dGhpcy5tb2RhbElucHV0ID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcignI21vZGFsX19pbnB1dF9pZCcpO1xyXG5cdFx0dGhpcy5vcGVuTW9kYWwgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcuanNfX29wZW5NeU1vZGFsJyk7XHJcblx0XHR0aGlzLmNyZWF0ZU1vZGFsID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLmpzX19teU1vZGFsQ3JlYXRlJyk7XHJcblx0XHR0aGlzLmNhcmRFbXB0eSA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5jYXJkX19lbXB0eScpO1xyXG5cdFx0dGhpcy5tb2RhbENvdW50ID0gMDtcclxuXHRcdHRoaXMuaW5wdXRUZXh0TGVnaHQgPSAzO1xyXG5cdH1cclxuXHJcblx0c3RhdGljIGluZm8oKSB7XHJcblx0XHRjb25zb2xlLmxvZygnTU9EVUxFOicsIHRoaXMubmFtZSwgdHJ1ZSk7XHJcblx0fVxyXG5cclxuXHRydW4oKSB7XHJcblx0XHRpZiAodGhpcy5zZWxlY3Rvcikge1xyXG5cdFx0XHR0aGlzLmNvbnN0cnVjdG9yLmluZm8oKTtcclxuXHJcblx0XHRcdGNvbnN0IHBhc3RIVE1MID0ge1xyXG5cdFx0XHRcdGNhcmQ6ICh0aXRsZU5hbWUsIHBhc3RDb3VudCkgPT4gKGBcclxuXHRcdFx0XHRcdFx0XHRcdFx0PGRpdiBjbGFzcz1cImNhcmRfX2hlYWRcIj5cclxuXHRcdFx0XHRcdFx0XHRcdFx0PGg0IGNsYXNzPVwiY2FyZF9fdGl0bGVcIj4ke3RpdGxlTmFtZX08L2g0PlxyXG5cdFx0XHRcdFx0XHRcdFx0XHQ8ZGl2IGNsYXNzPVwiY2FyZF9fbmF2XCI+XHJcblx0XHRcdFx0XHRcdFx0XHRcdDxkaXYgY2xhc3M9XCJjYXJkX19ncm91cFwiPlxyXG5cdFx0XHRcdFx0XHRcdFx0XHQ8ZGl2IGNsYXNzPVwiY2FyZF9faXRlbSBjYXJkX19pdGVtLXN0YXR1c1wiPjxzcGFuIGRhdGEtd29ya2VyLWNvdW50PiR7cGFzdENvdW50fTwvc3Bhbj48cD7QstCw0LrQsNC90YHQuNC4PC9wPjwvZGl2PlxyXG5cdFx0XHRcdFx0XHRcdFx0XHQ8ZGl2IGNsYXNzPVwiY2FyZF9faXRlbSBjYXJkX19pdGVtLWFkZCBhY3RpdmVcIiBkYXRhLXByb2plY3Q+0JTQvtCx0LDQstC40YLRjCDQstCw0LrQsNC90YHQuNGOPC9kaXY+XHJcblx0XHRcdFx0XHRcdFx0XHRcdDxkaXYgY2xhc3M9XCJjYXJkX19pdGVtIGNhcmRfX2l0ZW0tY2xvc2VcIiBkYXRhLXByb2plY3Q+XHJcblx0XHRcdFx0XHRcdFx0XHRcdDxzcGFuPjxzdmcgcm9sZT1cInBpY3R1cmUtc3ZnXCIgY2xhc3M9XCJnbHlwaHNfX2NoZWNrYm94XCI+PHVzZSB4bGluazpocmVmPVwiI2lkLWdseXBocy1jaGVja2JveFwiPjwvdXNlPjwvc3ZnPjwvc3Bhbj5cclxuXHRcdFx0XHRcdFx0XHRcdFx0PHAgY2xhc3M9XCJjYXJkX190ZXh0XCI+0J/RgNC+0LXQutGCINC30LDQutGA0YvRgiwg0YHQvtGC0YDRg9C00L3QuNC60Lgg0L3QsNC90Y/RgtGLPC9wPlxyXG5cdFx0XHRcdFx0XHRcdFx0XHQ8L2Rpdj5cclxuXHRcdFx0XHRcdFx0XHRcdFx0PC9kaXY+XHJcblx0XHRcdFx0XHRcdFx0XHRcdDxkaXYgY2xhc3M9XCJjYXJkX19ncm91cFwiPlxyXG5cdFx0XHRcdFx0XHRcdFx0XHQ8ZGl2IGNsYXNzPVwiY2FyZF9faXRlbSBjYXJkX19pdGVtLWV4aXQgYWN0aXZlXCIgZGF0YS1yZW9wZW4+0JfQsNC60YDRi9GC0Ywg0L/RgNC+0LXQutGCPC9kaXY+XHJcblx0XHRcdFx0XHRcdFx0XHRcdDxkaXYgY2xhc3M9XCJjYXJkX19pdGVtIGNhcmRfX2l0ZW0tcmVvcGVuXCIgZGF0YS1yZW9wZW4+0J7RgtC60YDRi9GC0Ywg0L/RgNC+0LXQutGCPC9kaXY+XHJcblx0XHRcdFx0XHRcdFx0XHRcdDxkaXYgY2xhc3M9XCJjYXJkX19pdGVtXCI+XHJcblx0XHRcdFx0XHRcdFx0XHRcdDxwIGNsYXNzPVwiY2FyZF9fdGV4dCBjYXJkX190ZXh0LWtpbGwgYWN0aXZlXCIgZGF0YS1zdGF0dXM+0KPQtNCw0LvQuNGC0Yw8L3A+XHJcblx0XHRcdFx0XHRcdFx0XHRcdDxkaXYgY2xhc3M9XCJjYXJkX19pdGVtLXJlbW92ZVwiIGRhdGEtc3RhdHVzPlxyXG5cdFx0XHRcdFx0XHRcdFx0XHQ8c3BhbiBkYXRhLXN0YXR1cy15ZXM+PHN2ZyByb2xlPVwicGljdHVyZS1zdmdcIiBjbGFzcz1cImdseXBoc19fY2hlY2tib3hcIj48dXNlIHhsaW5rOmhyZWY9XCIjaWQtZ2x5cGhzLWNoZWNrYm94XCI+PC91c2U+PC9zdmc+PC9zcGFuPlxyXG5cdFx0XHRcdFx0XHRcdFx0XHQ8c3BhbiBkYXRhLXN0YXR1cy1ub3BlPjxzdmcgcm9sZT1cInBpY3R1cmUtc3ZnXCIgY2xhc3M9XCJnbHlwaHNfX2Nsb3NlbW9kYWxcIj48dXNlIHhsaW5rOmhyZWY9XCIjaWQtZ2x5cGhzLWNsb3NlbW9kYWxcIj48L3VzZT48L3N2Zz48L3NwYW4+XHJcblx0XHRcdFx0XHRcdFx0XHRcdDwvZGl2PlxyXG5cdFx0XHRcdFx0XHRcdFx0XHQ8L2Rpdj5cclxuXHRcdFx0XHRcdFx0XHRcdFx0PC9kaXY+XHJcblx0XHRcdFx0XHRcdFx0XHRcdDwvZGl2PlxyXG5cdFx0XHRcdFx0XHRcdFx0XHQ8L2Rpdj5cclxuXHRcdFx0XHRcdFx0XHRcdGApLFxyXG5cdFx0XHRcdGNhcmRsaXN0OiB0aXRsZU5hbWUgPT4gKGBcclxuXHRcdFx0XHRcdFx0XHRcdFx0PGRpdiBjbGFzcz1cImNhcmQtbGlzdF9faGVhZFwiPlxyXG5cdFx0XHRcdFx0XHRcdFx0XHQ8aDQgY2xhc3M9XCJjYXJkLWxpc3RfX3RpdGxlIGFjdGl2ZVwiIGRhdGEtbGlzdC10aXRsZT7Qn9GD0YHRgtC+PC9oND5cclxuXHRcdFx0XHRcdFx0XHRcdFx0PGlucHV0IGNsYXNzPVwiY2FyZC1saXN0X190aXRsZSBjYXJkLWxpc3RfX3RpdGxlLWVkaXRcIiB0eXBlPVwidGV4dFwiIG5hbWU9XCJ0ZXh0XCIgYXV0b2NvbXBsZXRlPVwib2ZmXCIgbWF4bGVuZ3RoPVwiMTIwXCI+XHJcblx0XHRcdFx0XHRcdFx0XHRcdDwvZGl2PlxyXG5cdFx0XHRcdFx0XHRcdFx0XHQ8ZGl2IGNsYXNzPVwiY2FyZC1saXN0X19uYXZcIj5cclxuXHRcdFx0XHRcdFx0XHRcdFx0PGRpdiBjbGFzcz1cImNhcmQtbGlzdF9fZ3JvdXBcIj5cclxuXHRcdFx0XHRcdFx0XHRcdFx0PGRpdiBjbGFzcz1cImNhcmQtbGlzdF9faXRlbSBjYXJkLWxpc3RfX2l0ZW0tc2VhcmNoIGFjdGl2ZVwiIGRhdGEtbGlzdC1wcm9qZWN0PlxyXG5cdFx0XHRcdFx0XHRcdFx0XHQ8c3Bhbj48c3ZnIHJvbGU9XCJwaWN0dXJlLXN2Z1wiIGNsYXNzPVwiZ2x5cGhzX19zZWFyY2hcIj48dXNlIHhsaW5rOmhyZWY9XCIjaWQtZ2x5cGhzLXNlYXJjaFwiPjwvdXNlPjwvc3ZnPjwvc3Bhbj5cclxuXHRcdFx0XHRcdFx0XHRcdFx0PHAgY2xhc3M9XCJjYXJkLWxpc3RfX3RleHRcIj7QktCw0LrQsNC90YHQuNGPINC+0YLQutGA0YvRgtCwLCDQuNC00LXRgiDQv9C+0LTQsdC+0YAg0LrQsNC90LTQuNC00LDRgtC+0LI8L3A+XHJcblx0XHRcdFx0XHRcdFx0XHRcdDwvZGl2PlxyXG5cdFx0XHRcdFx0XHRcdFx0XHQ8ZGl2IGNsYXNzPVwiY2FyZC1saXN0X19pdGVtIGNhcmQtbGlzdF9faXRlbS1zZWFyY2hcIiBkYXRhLWxpc3QtcHJvamVjdD5cclxuXHRcdFx0XHRcdFx0XHRcdFx0PHNwYW4+PHN2ZyByb2xlPVwicGljdHVyZS1zdmdcIiBjbGFzcz1cImdseXBoc19fY2hlY2tib3hcIj48dXNlIHhsaW5rOmhyZWY9XCIjaWQtZ2x5cGhzLWNoZWNrYm94XCI+PC91c2U+PC9zdmc+PC9zcGFuPlxyXG5cdFx0XHRcdFx0XHRcdFx0XHQ8cCBjbGFzcz1cImNhcmQtbGlzdF9fdGV4dFwiPtCS0LDQutCw0L3RgdC40Y8g0LfQsNC60YDRi9GC0LAsINGB0L7RgtGA0YPQtNC90LjQuiDQvdCw0L3Rj9GCPC9wPlxyXG5cdFx0XHRcdFx0XHRcdFx0XHQ8L2Rpdj5cclxuXHRcdFx0XHRcdFx0XHRcdFx0PC9kaXY+XHJcblx0XHRcdFx0XHRcdFx0XHRcdDxkaXYgY2xhc3M9XCJjYXJkLWxpc3RfX2dyb3VwIGpzX190b2dnbGVMaXN0UmVvcGVuXCI+XHJcblx0XHRcdFx0XHRcdFx0XHRcdDxkaXYgY2xhc3M9XCJjYXJkLWxpc3RfX2l0ZW0gY2FyZC1saXN0X19pdGVtLWV4aXQgYWN0aXZlXCIgZGF0YS1saXN0LXJlb3Blbj7Ql9Cw0LrRgNGL0YLRjCDQstCw0LrQsNC90YHQuNGOPC9kaXY+XHJcblx0XHRcdFx0XHRcdFx0XHRcdDxkaXYgY2xhc3M9XCJjYXJkLWxpc3RfX2l0ZW0gY2FyZC1saXN0X19pdGVtLXJlb3BlblwiIGRhdGEtbGlzdC1yZW9wZW4+0J7RgtC60YDRi9GC0Ywg0LLQsNC60LDQvdGB0LjRjjwvZGl2PlxyXG5cdFx0XHRcdFx0XHRcdFx0XHQ8ZGl2IGNsYXNzPVwiY2FyZC1saXN0X19pdGVtIGpzX190b2dnbGVMaXN0S2lsbFwiPlxyXG5cdFx0XHRcdFx0XHRcdFx0XHQ8cCBjbGFzcz1cImNhcmQtbGlzdF9fdGV4dCBjYXJkLWxpc3RfX3RleHQta2lsbCBhY3RpdmVcIiBkYXRhLWxpc3Qtc3RhdHVzPtCj0LTQsNC70LjRgtGMPC9wPlxyXG5cdFx0XHRcdFx0XHRcdFx0XHQ8ZGl2IGNsYXNzPVwiY2FyZC1saXN0X19pdGVtLXJlbW92ZVwiIGRhdGEtbGlzdC1zdGF0dXM+XHJcblx0XHRcdFx0XHRcdFx0XHRcdDxzcGFuIGRhdGEtbGlzdC1zdGF0dXMteWVzPjxzdmcgcm9sZT1cInBpY3R1cmUtc3ZnXCIgY2xhc3M9XCJnbHlwaHNfX2NoZWNrYm94XCI+PHVzZSB4bGluazpocmVmPVwiI2lkLWdseXBocy1jaGVja2JveFwiPjwvdXNlPjwvc3ZnPjwvc3Bhbj5cclxuXHRcdFx0XHRcdFx0XHRcdFx0PHNwYW4gZGF0YS1saXN0LXN0YXR1cy1ub3BlPjxzdmcgcm9sZT1cInBpY3R1cmUtc3ZnXCIgY2xhc3M9XCJnbHlwaHNfX2Nsb3NlbW9kYWxcIj48dXNlIHhsaW5rOmhyZWY9XCIjaWQtZ2x5cGhzLWNsb3NlbW9kYWxcIj48L3VzZT48L3N2Zz48L3NwYW4+XHJcblx0XHRcdFx0XHRcdFx0XHRcdDwvZGl2PlxyXG5cdFx0XHRcdFx0XHRcdFx0XHQ8L2Rpdj5cclxuXHRcdFx0XHRcdFx0XHRcdFx0PC9kaXY+XHJcblx0XHRcdFx0XHRcdFx0XHRcdDwvZGl2PlxyXG5cdFx0XHRcdFx0XHRcdFx0YClcclxuXHRcdFx0fTtcclxuXHJcblx0XHRcdGNvbnN0IG9ic2VydmVyQ2FyZENvdW50ID0gKCkgPT4ge1xyXG5cdFx0XHRcdGlmICh0aGlzLnNlbGVjdG9yLnF1ZXJ5U2VsZWN0b3IoJy5qc19fdG9nZ2xlU3RhdHVzJykgPD0gMCkge1xyXG5cdFx0XHRcdFx0dGhpcy5jYXJkRW1wdHkuY2xhc3NMaXN0LmFkZCgnYWN0aXZlJyk7XHJcblx0XHRcdFx0fSBlbHNlIHRoaXMuY2FyZEVtcHR5LmNsYXNzTGlzdC5yZW1vdmUoJ2FjdGl2ZScpO1xyXG5cdFx0XHR9O1xyXG5cdFx0XHRjb25zdCBvYnNlcnZlckNhcmQgPSBuZXcgTXV0YXRpb25PYnNlcnZlcihvYnNlcnZlckNhcmRDb3VudCk7XHJcblx0XHRcdG9ic2VydmVyQ2FyZC5vYnNlcnZlKHRoaXMuc2VsZWN0b3IsIHsgY2hpbGRMaXN0OiB0cnVlIH0sIGZhbHNlKTtcclxuXHJcblx0XHRcdGNvbnN0IHVwZGF0ZUxpc3RDb3VudCA9IChldmVudCkgPT4ge1xyXG5cdFx0XHRcdGNvbnN0IGxpc3QgPSBbLi4uZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbCgnLmpzX190b2dnbGVTdGF0dXMnKV07XHJcblx0XHRcdFx0bGlzdC5mb3JFYWNoKChmKSA9PiB7XHJcblx0XHRcdFx0XHRjb25zdCBjb3VudCA9IGYuY2hpbGRFbGVtZW50Q291bnQgLSAxO1xyXG5cdFx0XHRcdFx0Y29uc3QgaXRlbSA9IGYucXVlcnlTZWxlY3RvcignW2RhdGEtd29ya2VyLWNvdW50XScpO1xyXG5cdFx0XHRcdFx0aXRlbS50ZXh0Q29udGVudCA9IGNvdW50IHx8IDA7XHJcblx0XHRcdFx0fSk7XHJcblx0XHRcdH07XHJcblx0XHRcdHVwZGF0ZUxpc3RDb3VudCgpO1xyXG5cclxuXHRcdFx0Y29uc3Qgb3Blbk1vZGFsU3RhdGUgPSAoZXZlbnQpID0+IHtcclxuXHRcdFx0XHRjb25zdCBfdGFyZ2V0ID0gZXZlbnQudGFyZ2V0O1xyXG5cdFx0XHRcdGNvbnN0IGFuaW1hRW5kID0gKCkgPT4ge1xyXG5cdFx0XHRcdFx0dGhpcy5vcGVuTW9kYWwuY2xhc3NMaXN0LnJlbW92ZSgnYW5pbWEtb3BhY2l0eS1oaWRlJywgJ2FjdGl2ZScpO1xyXG5cdFx0XHRcdFx0dGhpcy5tb2RhbElucHV0LmNsYXNzTGlzdC5yZW1vdmUoJ2Vycm9yJyk7XHJcblx0XHRcdFx0XHR0aGlzLm1vZGFsSW5wdXQudmFsdWUgPSAnJztcclxuXHRcdFx0XHRcdHRoaXMubW9kYWxJbnB1dC5uZXh0RWxlbWVudFNpYmxpbmcudGV4dENvbnRlbnQgPSAnJztcclxuXHRcdFx0XHR9O1xyXG5cdFx0XHRcdFxyXG5cdFx0XHRcdGlmIChfdGFyZ2V0Lm1hdGNoZXMoJy5qc19fb3Blbk15TW9kYWwuYWN0aXZlJykgfHwgX3RhcmdldC5tYXRjaGVzKCcubW9kYWxfX2Nsb3NlJykpIHtcclxuXHRcdFx0XHRcdHRoaXMub3Blbk1vZGFsLmNsYXNzTGlzdC5hZGQoJ2FuaW1hLW9wYWNpdHktaGlkZScpO1xyXG5cdFx0XHRcdFx0dGhpcy5vcGVuTW9kYWwuYWRkRXZlbnRMaXN0ZW5lcignYW5pbWF0aW9uZW5kJywgYW5pbWFFbmQsIHsgb25jZTogdHJ1ZSB9LCBmYWxzZSk7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9O1xyXG5cdFx0XHRjb25zdCBhZGRidXR0b25PcGFjaXR5ID0gKCkgPT4ge1xyXG5cdFx0XHRcdGNvbnN0IGFuaW1hRW5kID0gKCkgPT4gdGhpcy5vcGVuTW9kYWwuY2xhc3NMaXN0LnJlbW92ZSgnYW5pbWEtb3BhY2l0eS1zaG93Jyk7XHJcblx0XHRcdFx0XHJcblx0XHRcdFx0dGhpcy5vcGVuTW9kYWwuY2xhc3NMaXN0LmFkZCgnYWN0aXZlJyk7XHJcblx0XHRcdFx0dGhpcy5vcGVuTW9kYWwuY2xhc3NMaXN0LmFkZCgnYW5pbWEtb3BhY2l0eS1zaG93Jyk7XHJcblx0XHRcdFx0dGhpcy5vcGVuTW9kYWwuYWRkRXZlbnRMaXN0ZW5lcignYW5pbWF0aW9uZW5kJywgYW5pbWFFbmQsIHsgb25jZTogdHJ1ZSB9LCBmYWxzZSk7XHJcblx0XHRcdH07XHJcblxyXG5cdFx0XHRjb25zdCBjcmVhdGVOZXdUYXNrID0gKCkgPT4ge1xyXG5cdFx0XHRcdGNvbnN0IHBhc3RJbnB1dCA9IHRoaXMubW9kYWxJbnB1dC52YWx1ZSB8fCAn0J/Rg9GB0YLQvic7XHJcblx0XHRcdFx0Y29uc3QgcGFzdENvdW50ID0gdGhpcy5tb2RhbENvdW50O1xyXG5cdFx0XHRcdGNvbnN0IGFuaW1hRW5kID0gKCkgPT4gY3JlYXRlRGl2LmNsYXNzTGlzdC5yZW1vdmUoJ2FuaW1hLWFkZC1tZScpO1xyXG5cclxuXHRcdFx0XHRjb25zdCBjcmVhdGVEaXYgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdESVYnKTtcclxuXHRcdFx0XHRjcmVhdGVEaXYuY2xhc3NMaXN0LmFkZCgnY2FyZF9fYm94JywgJ2pzX190b2dnbGVTdGF0dXMnLCAnYW5pbWEtYWRkLW1lJyk7XHJcblx0XHRcdFx0Y3JlYXRlRGl2LmlubmVySFRNTCA9IHBhc3RIVE1MLmNhcmQocGFzdElucHV0LCBwYXN0Q291bnQpO1xyXG5cdFx0XHRcdHRoaXMuc2VsZWN0b3IuYXBwZW5kQ2hpbGQoY3JlYXRlRGl2KTtcclxuXHRcdFx0XHR0aGlzLm9wZW5Nb2RhbC5jbGFzc0xpc3QudG9nZ2xlKCdhY3RpdmUnKTtcclxuXHRcdFx0XHR0aGlzLm1vZGFsSW5wdXQudmFsdWUgPSAnJztcclxuXHRcdFx0XHRjcmVhdGVEaXYuYWRkRXZlbnRMaXN0ZW5lcignYW5pbWF0aW9uZW5kJywgYW5pbWFFbmQsIHsgb25jZTogdHJ1ZSB9LCBmYWxzZSk7XHJcblx0XHRcdH07XHJcblxyXG5cdFx0XHRjb25zdCBjcmVhdGVOZXdUYXNrSW5wdXRWYWxpZGF0ID0gKCkgPT4ge1xyXG5cdFx0XHRcdGlmICh0aGlzLm1vZGFsSW5wdXQudmFsdWUubGVuZ3RoID49IHRoaXMuaW5wdXRUZXh0TGVnaHQpIHtcclxuXHRcdFx0XHRcdHRoaXMubW9kYWxJbnB1dC5jbGFzc0xpc3QucmVtb3ZlKCdlcnJvcicpO1xyXG5cdFx0XHRcdFx0dGhpcy5tb2RhbElucHV0Lm5leHRFbGVtZW50U2libGluZy50ZXh0Q29udGVudCA9ICcnO1xyXG5cdFx0XHRcdH0gXHJcblx0XHRcdH07XHJcblx0XHRcdFxyXG5cdFx0XHRjb25zdCBjcmVhdGVOZXdUYXNrVmFsaWRhdCA9ICgpID0+IHtcclxuXHRcdFx0XHRpZiAodGhpcy5tb2RhbElucHV0LnZhbHVlLmxlbmd0aCA8PSB0aGlzLmlucHV0VGV4dExlZ2h0KSB7XHJcblx0XHRcdFx0XHR0aGlzLm1vZGFsSW5wdXQuY2xhc3NMaXN0LmFkZCgnZXJyb3InKTtcclxuXHRcdFx0XHRcdHRoaXMubW9kYWxJbnB1dC5uZXh0RWxlbWVudFNpYmxpbmcudGV4dENvbnRlbnQgPSBg0JLQstC10LTQtdC90L3Ri9C1INC00LDQvdC90YvQtSDQvNC10L3RjNGI0LUgJHt0aGlzLmlucHV0VGV4dExlZ2h0fSDRgdC40LzQstC+0LvQvtCyYDtcclxuXHRcdFx0XHRcdHJldHVybjtcclxuXHRcdFx0XHR9XHJcblx0XHRcdFx0Y3JlYXRlTmV3VGFzaygpO1xyXG5cdFx0XHR9O1xyXG5cclxuXHRcdFx0Y29uc3QgY3JlYXRlTmV3TGlzdCA9IChldmVudCkgPT4ge1xyXG5cdFx0XHRcdGNvbnN0IG15VGFyZ2V0ID0gZXZlbnQudGFyZ2V0LmNsb3Nlc3QoJy5qc19fdG9nZ2xlU3RhdHVzJyk7XHJcblxyXG5cdFx0XHRcdGlmIChteVRhcmdldCAmJiBldmVudC50YXJnZXQubWF0Y2hlcygnW2RhdGEtcHJvamVjdF0nKSkge1xyXG5cdFx0XHRcdFx0Y29uc3QgY291bnQgPSBteVRhcmdldC5jaGlsZEVsZW1lbnRDb3VudCB8fCAwO1xyXG5cdFx0XHRcdFx0Y29uc3QgY3JlYXRlRGl2ID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnRElWJyk7XHJcblx0XHRcdFx0XHRjcmVhdGVEaXYuY2xhc3NMaXN0LmFkZCgnY2FyZC1saXN0X19ib3gnKTtcclxuXHRcdFx0XHRcdGNyZWF0ZURpdi5pbm5lckhUTUwgKz0gcGFzdEhUTUwuY2FyZGxpc3QoJ9Cd0LDQttC80Lgg0LzQtdC90Y8hJyk7XHJcblxyXG5cdFx0XHRcdFx0ZXZlbnQudGFyZ2V0LmNsb3Nlc3QoJy5jYXJkX19ib3gnKS5hcHBlbmRDaGlsZChjcmVhdGVEaXYpO1xyXG5cdFx0XHRcdFx0bXlUYXJnZXQucXVlcnlTZWxlY3RvcignW2RhdGEtd29ya2VyLWNvdW50XScpLnRleHRDb250ZW50ID0gY291bnQ7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9O1xyXG5cclxuXHRcdFx0Y29uc3QgdG9nZ2xlQWN0aXZlQ2xhc3MgPSAoZXZlbnQpID0+IHtcclxuXHRcdFx0XHRjb25zdCBteVRhcmdldCA9IGV2ZW50LnRhcmdldC5jbG9zZXN0KCcuanNfX3RvZ2dsZVN0YXR1cycpO1xyXG5cdFx0XHRcdGNvbnN0IG15TGlzdFRhcmdldFJlb3BlbiA9IGV2ZW50LnRhcmdldC5jbG9zZXN0KCcuanNfX3RvZ2dsZUxpc3RSZW9wZW4nKTtcclxuXHRcdFx0XHRjb25zdCBteUxpc3RUYXJnZXRLaWxsID0gZXZlbnQudGFyZ2V0LmNsb3Nlc3QoJy5qc19fdG9nZ2xlTGlzdEtpbGwnKTtcclxuXHJcblx0XHRcdFx0aWYgKG15VGFyZ2V0ICYmIGV2ZW50LnRhcmdldC5tYXRjaGVzKCdbZGF0YS1saXN0LXRpdGxlXScpKSB7XHJcblx0XHRcdFx0XHRjb25zdCBfdGFyZ2V0ID0gZXZlbnQudGFyZ2V0O1xyXG5cdFx0XHRcdFx0Y29uc3QgaW5wdXRUZXh0ID0gZXZlbnQudGFyZ2V0Lm5leHRFbGVtZW50U2libGluZztcclxuXHRcdFx0XHRcdGlucHV0VGV4dC52YWx1ZSA9IF90YXJnZXQudGV4dENvbnRlbnQ7XHJcblxyXG5cdFx0XHRcdFx0X3RhcmdldC5jbGFzc0xpc3QudG9nZ2xlKCdhY3RpdmUnKTtcclxuXHRcdFx0XHRcdF90YXJnZXQubmV4dEVsZW1lbnRTaWJsaW5nLmZvY3VzKCk7XHJcblx0XHRcdFx0XHRfdGFyZ2V0Lm5leHRFbGVtZW50U2libGluZy5zZWxlY3QoKTtcclxuXHJcblx0XHRcdFx0XHRjb25zdCBteUZvY3VzRnVuYyA9IChldikgPT4geyBpZiAoZXYua2V5ID09PSAnRW50ZXInKSBpbnB1dFRleHQuYmx1cigpOyB9O1xyXG5cdFx0XHRcdFx0Y29uc3QgbXlCbHVyT3V0ID0gKCkgPT4ge1xyXG5cdFx0XHRcdFx0XHRpZiAoZXZlbnQuZGVmYXVsdFByZXZlbnRlZCkgcmV0dXJuO1xyXG5cdFx0XHRcdFx0XHRfdGFyZ2V0LnRleHRDb250ZW50ID0gaW5wdXRUZXh0LnZhbHVlIHx8ICfQn9GD0YHRgtC+JztcclxuXHRcdFx0XHRcdFx0c2V0VGltZW91dCgoKSA9PiBfdGFyZ2V0LmNsYXNzTGlzdC50b2dnbGUoJ2FjdGl2ZScpLCAyNTApO1xyXG5cdFx0XHRcdFx0XHRpbnB1dFRleHQucmVtb3ZlRXZlbnRMaXN0ZW5lcigna2V5cHJlc3MnLCBteUZvY3VzRnVuYyk7XHJcblx0XHRcdFx0XHR9O1xyXG5cclxuXHRcdFx0XHRcdGlucHV0VGV4dC5hZGRFdmVudExpc3RlbmVyKCdibHVyJywgbXlCbHVyT3V0LCB7IG9uY2U6IHRydWUgfSwgZmFsc2UpO1xyXG5cdFx0XHRcdFx0aW5wdXRUZXh0LmFkZEV2ZW50TGlzdGVuZXIoJ2tleXByZXNzJywgbXlGb2N1c0Z1bmMsIGZhbHNlKTtcclxuXHRcdFx0XHR9XHJcblxyXG5cdFx0XHRcdGlmIChteVRhcmdldCAmJiAoZXZlbnQudGFyZ2V0Lm1hdGNoZXMoJ1tkYXRhLXN0YXR1c10nKSB8fCBldmVudC50YXJnZXQubWF0Y2hlcygnW2RhdGEtc3RhdHVzLW5vcGVdJykpKSB7XHJcblx0XHRcdFx0XHRbLi4ubXlUYXJnZXQucXVlcnlTZWxlY3RvckFsbCgnW2RhdGEtc3RhdHVzXScpXS5mb3JFYWNoKGYgPT4gZi5jbGFzc0xpc3QudG9nZ2xlKCdhY3RpdmUnKSk7XHJcblx0XHRcdFx0fVxyXG5cclxuXHRcdFx0XHRpZiAobXlUYXJnZXQgJiYgZXZlbnQudGFyZ2V0Lm1hdGNoZXMoJ1tkYXRhLXN0YXR1cy15ZXNdJykpIHtcclxuXHRcdFx0XHRcdGNvbnN0IG15Q2FyZCA9IGV2ZW50LnRhcmdldC5jbG9zZXN0KCcuY2FyZF9fYm94Jyk7XHJcblx0XHRcdFx0XHRjb25zdCBhbmltYUVuZCA9ICgpID0+IG15Q2FyZC5yZW1vdmUoKTtcclxuXHRcdFx0XHRcdFxyXG5cdFx0XHRcdFx0bXlDYXJkLnN0eWxlLm1heEhlaWdodCA9IGAke215Q2FyZC5vZmZzZXRIZWlnaHR9cHhgO1xyXG5cdFx0XHRcdFx0bXlDYXJkLmNsYXNzTGlzdC5hZGQoJ2FuaW1hLXJlbW92ZS1tZScpO1xyXG5cdFx0XHRcdFx0bXlDYXJkLmFkZEV2ZW50TGlzdGVuZXIoJ2FuaW1hdGlvbmVuZCcsIGFuaW1hRW5kLCB7IG9uY2U6IHRydWUgfSwgZmFsc2UpO1xyXG5cdFx0XHRcdH1cclxuXHJcblx0XHRcdFx0aWYgKG15VGFyZ2V0ICYmIGV2ZW50LnRhcmdldC5tYXRjaGVzKCdbZGF0YS1yZW9wZW5dJykpIHtcclxuXHRcdFx0XHRcdFsuLi5teVRhcmdldC5xdWVyeVNlbGVjdG9yQWxsKCdbZGF0YS1yZW9wZW5dJyldLmZvckVhY2goZiA9PiBmLmNsYXNzTGlzdC50b2dnbGUoJ2FjdGl2ZScpKTtcclxuXHRcdFx0XHRcdFsuLi5teVRhcmdldC5xdWVyeVNlbGVjdG9yQWxsKCdbZGF0YS1wcm9qZWN0XScpXS5mb3JFYWNoKGYgPT4gZi5jbGFzc0xpc3QudG9nZ2xlKCdhY3RpdmUnKSk7XHJcblx0XHRcdFx0fVxyXG5cclxuXHRcdFx0XHRpZiAobXlMaXN0VGFyZ2V0S2lsbCAmJiAoZXZlbnQudGFyZ2V0Lm1hdGNoZXMoJ1tkYXRhLWxpc3Qtc3RhdHVzXScpIHx8IGV2ZW50LnRhcmdldC5tYXRjaGVzKCdbZGF0YS1saXN0LXN0YXR1cy1ub3BlXScpKSkge1xyXG5cdFx0XHRcdFx0Wy4uLm15TGlzdFRhcmdldEtpbGwucXVlcnlTZWxlY3RvckFsbCgnW2RhdGEtbGlzdC1zdGF0dXNdJyldLmZvckVhY2goZiA9PiBmLmNsYXNzTGlzdC50b2dnbGUoJ2FjdGl2ZScpKTtcclxuXHRcdFx0XHR9XHJcblxyXG5cdFx0XHRcdGlmIChteUxpc3RUYXJnZXRLaWxsICYmIGV2ZW50LnRhcmdldC5tYXRjaGVzKCdbZGF0YS1saXN0LXN0YXR1cy15ZXNdJykpIHtcclxuXHRcdFx0XHRcdGNvbnN0IGxpc3RDb3VudCA9IG15VGFyZ2V0LmNoaWxkRWxlbWVudENvdW50IC0gMiB8fCAwO1xyXG5cdFx0XHRcdFx0Y29uc3QgbXlDYXJkTGlzdCA9IGV2ZW50LnRhcmdldC5jbG9zZXN0KCcuY2FyZC1saXN0X19ib3gnKTtcclxuXHRcdFx0XHRcdG15Q2FyZExpc3QuY2xhc3NMaXN0LmFkZCgnYW5pbWEtcmVtb3ZlLW1lJyk7XHJcblx0XHRcdFx0XHRteUNhcmRMaXN0LmFkZEV2ZW50TGlzdGVuZXIoJ2FuaW1hdGlvbmVuZCcsICgpID0+IG15Q2FyZExpc3QucmVtb3ZlKCksIHsgb25jZTogdHJ1ZSB9LCBmYWxzZSk7XHJcblx0XHRcdFx0XHRteVRhcmdldC5xdWVyeVNlbGVjdG9yKCdbZGF0YS13b3JrZXItY291bnRdJykudGV4dENvbnRlbnQgPSBsaXN0Q291bnQ7XHJcblx0XHRcdFx0fVxyXG5cclxuXHRcdFx0XHRpZiAobXlMaXN0VGFyZ2V0UmVvcGVuICYmIGV2ZW50LnRhcmdldC5tYXRjaGVzKCdbZGF0YS1saXN0LXJlb3Blbl0nKSkge1xyXG5cdFx0XHRcdFx0Y29uc3QgbXlDYXJkTGlzdCA9IGV2ZW50LnRhcmdldC5jbG9zZXN0KCcuY2FyZC1saXN0X19ib3gnKTtcclxuXHRcdFx0XHRcdFsuLi5teUxpc3RUYXJnZXRSZW9wZW4ucXVlcnlTZWxlY3RvckFsbCgnW2RhdGEtbGlzdC1yZW9wZW5dJyldLmZvckVhY2goZiA9PiBmLmNsYXNzTGlzdC50b2dnbGUoJ2FjdGl2ZScpKTtcclxuXHRcdFx0XHRcdFsuLi5teUxpc3RUYXJnZXRSZW9wZW4ucGFyZW50RWxlbWVudC5xdWVyeVNlbGVjdG9yQWxsKCdbZGF0YS1saXN0LXByb2plY3RdJyldLmZvckVhY2goZiA9PiBmLmNsYXNzTGlzdC50b2dnbGUoJ2FjdGl2ZScpKTtcclxuXHJcblx0XHRcdFx0XHRteUNhcmRMaXN0LnF1ZXJ5U2VsZWN0b3IoJ1tkYXRhLWxpc3QtdGl0bGVdJykuY2xhc3NMaXN0LnRvZ2dsZSgncG9pbnRlcmV2ZW50cycpO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0fTtcclxuXHJcblx0XHRcdHRoaXMuYWRkYnV0dG9uLmFkZEV2ZW50TGlzdGVuZXIoJ2NsaWNrJywgYWRkYnV0dG9uT3BhY2l0eSwgZmFsc2UpO1xyXG5cdFx0XHR0aGlzLm9wZW5Nb2RhbC5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsIG9wZW5Nb2RhbFN0YXRlLCBmYWxzZSk7XHJcblx0XHRcdHRoaXMubW9kYWxJbnB1dC5hZGRFdmVudExpc3RlbmVyKCdpbnB1dCcsIGNyZWF0ZU5ld1Rhc2tJbnB1dFZhbGlkYXQsIGZhbHNlKTtcclxuXHRcdFx0dGhpcy5jcmVhdGVNb2RhbC5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsIGNyZWF0ZU5ld1Rhc2tWYWxpZGF0LCBmYWxzZSk7XHJcblx0XHRcdHRoaXMuc2VsZWN0b3IuYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCBjcmVhdGVOZXdMaXN0LCBmYWxzZSk7XHJcblx0XHRcdHRoaXMuc2VsZWN0b3IuYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCB0b2dnbGVBY3RpdmVDbGFzcywgZmFsc2UpO1xyXG5cclxuXHRcdH1cclxuXHR9XHJcbn07XHJcbiIsIm1vZHVsZS5leHBvcnRzID0gY2xhc3MgQWRkTW92ZVNjcm9sbFVQIHtcclxuXHRjb25zdHJ1Y3Rvcihpbml0KSB7XHJcblx0XHR0aGlzLnNlbGVjdG9yID0gaW5pdC5zZWxlY3RvcjtcclxuXHRcdHRoaXMuY291bnQgPSBOdW1iZXIoTWF0aC5hYnMoaW5pdC5zcGVlZCkpIHx8IDg7XHJcblx0XHR0aGlzLnNwZWVkID0gKHRoaXMuY291bnQgPD0gMjApID8gdGhpcy5jb3VudCA6IDg7XHJcblx0XHR0aGlzLm15UG9zID0gd2luZG93LnBhZ2VZT2Zmc2V0O1xyXG5cdFx0dGhpcy5nZXRTY3JvbGwgPSBkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQuc2Nyb2xsVG9wO1xyXG5cdFx0dGhpcy5zcGVlZFNjcm9sbCA9IHRoaXMuc3BlZWQ7XHJcblx0fVxyXG5cclxuXHRzdGF0aWMgaW5mbygpIHsgY29uc29sZS5sb2coJ01PRFVMRTonLCB0aGlzLm5hbWUsIHRydWUpOyB9XHJcblxyXG5cdHJ1bigpIHtcclxuXHRcdGNvbnN0IHNlbGVjdG9yID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihgJHt0aGlzLnNlbGVjdG9yfWApO1xyXG5cclxuXHRcdGlmIChzZWxlY3Rvcikge1xyXG5cdFx0XHR0aGlzLmNvbnN0cnVjdG9yLmluZm8oKTtcclxuXHJcblx0XHRcdGNvbnN0IGNsaWNrZWRBcnJvdyA9ICgpID0+IHtcclxuXHRcdFx0XHR0aGlzLmdldFNjcm9sbCA9IGRvY3VtZW50LmRvY3VtZW50RWxlbWVudC5zY3JvbGxUb3A7XHJcblx0XHRcdFx0aWYgKHRoaXMuZ2V0U2Nyb2xsID49IDEpIHtcclxuXHRcdFx0XHRcdHdpbmRvdy5yZXF1ZXN0QW5pbWF0aW9uRnJhbWUoY2xpY2tlZEFycm93KTtcclxuXHRcdFx0XHRcdHdpbmRvdy5zY3JvbGxUbygwLCB0aGlzLmdldFNjcm9sbCAtIHRoaXMuZ2V0U2Nyb2xsIC8gdGhpcy5zcGVlZFNjcm9sbCk7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9O1xyXG5cclxuXHRcdFx0Y29uc3Qgc2Nyb2xsZWREb3duID0gKCkgPT4ge1xyXG5cdFx0XHRcdHRoaXMubXlQb3MgPSB3aW5kb3cucGFnZVlPZmZzZXQ7XHJcblx0XHRcdFx0KHRoaXMubXlQb3MgPj0gMTAwKSA/IHNlbGVjdG9yLmNsYXNzTGlzdC5hZGQoJ29wZW4nKSA6IHNlbGVjdG9yLmNsYXNzTGlzdC5yZW1vdmUoJ29wZW4nKTtcclxuXHRcdFx0fTtcclxuXHJcblx0XHRcdHNlbGVjdG9yLmFkZEV2ZW50TGlzdGVuZXIoJ2NsaWNrJywgY2xpY2tlZEFycm93KTtcclxuXHRcdFx0d2luZG93LmFkZEV2ZW50TGlzdGVuZXIoJ3Njcm9sbCcsIHNjcm9sbGVkRG93bik7XHJcblxyXG5cdFx0fVxyXG5cdH1cclxufTtcclxuIiwibW9kdWxlLmV4cG9ydHMgPSBjbGFzcyBBZGRPcGVuTXlCdXJnZXIge1xyXG5cdGNvbnN0cnVjdG9yKGluaXQpIHtcclxuXHRcdHRoaXMuc2VsZWN0b3IgPSBpbml0LmJ1cmdlcjtcclxuXHRcdHRoaXMubmF2YmFyID0gaW5pdC5uYXZiYXI7XHJcblx0XHR0aGlzLmVycm9yVGV4dCA9ICfwn5KAIE91Y2gsIGRhdGFiYXNlIGVycm9yLi4uJztcclxuXHR9XHJcblxyXG5cdHN0YXRpYyBpbmZvKCkge1xyXG5cdFx0Y29uc29sZS5sb2coJ01PRFVMRTonLCB0aGlzLm5hbWUsIHRydWUpO1xyXG5cdH1cclxuXHJcblx0cnVuKCkge1xyXG5cdFx0Y29uc3Qgc2VsZWN0b3IgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKGAke3RoaXMuc2VsZWN0b3J9YCk7XHJcblx0XHRpZiAoc2VsZWN0b3IpIHtcclxuXHRcdFx0dGhpcy5jb25zdHJ1Y3Rvci5pbmZvKCk7XHJcblxyXG5cdFx0XHRjb25zdCBvcGVuQnVyZ2VyID0gKCkgPT4ge1xyXG5cdFx0XHRcdGNvbnN0IG5hdiA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoYCR7dGhpcy5uYXZiYXJ9YCk7XHJcblx0XHRcdFx0bmF2LnF1ZXJ5U2VsZWN0b3IoJ0RJVicpLmNsYXNzTGlzdC50b2dnbGUoJ29wZW4nKTtcclxuXHRcdFx0XHRzZWxlY3Rvci5jbGFzc0xpc3QudG9nZ2xlKCdvcGVuJyk7XHJcblx0XHRcdH07XHJcblxyXG5cdFx0XHRzZWxlY3Rvci5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsIG9wZW5CdXJnZXIpO1xyXG5cdFx0fVxyXG5cdH1cclxufTtcclxuIl0sIm1hcHBpbmdzIjoiO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7OztBQ2xGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUZBO0FBS0E7QUFDQTtBQUNBO0FBRkE7QUFLQTtBQUNBO0FBQ0E7QUFGQTtBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUM5QkE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQWJBO0FBQUE7QUFBQTtBQWtCQTtBQUNBO0FBQUE7QUFDQTtBQUVBO0FBQ0E7QUFBQTtBQUFBO0FBMEJBO0FBQUE7QUFBQTtBQTNCQTtBQUNBO0FBeURBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUE3T0E7QUFBQTtBQUFBO0FBZUE7QUFDQTtBQWhCQTtBQUNBO0FBREE7QUFBQTs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBVEE7QUFBQTtBQUFBO0FBWUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFuQ0E7QUFBQTtBQUFBO0FBVUE7QUFBQTtBQVZBO0FBQ0E7QUFEQTtBQUFBOzs7Ozs7Ozs7Ozs7Ozs7OztBQ0FBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFOQTtBQUFBO0FBQUE7QUFXQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQXhCQTtBQUFBO0FBQUE7QUFRQTtBQUNBO0FBVEE7QUFDQTtBQURBO0FBQUE7Ozs7QSIsInNvdXJjZVJvb3QiOiIifQ==