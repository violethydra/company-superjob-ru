// ============================
//    Name: index.js
// ============================

import AddMoveScrollUP from './modules/moveScrollUP';
import AddOpenMyBurger from './modules/openMyBurger';
import AddMyWorker from './modules/AddMyWorker';

const start = () => {
	console.log('DOM:', 'DOMContentLoaded', true);

	new AddMoveScrollUP({
		selector: '.js__moveScrollUP',
		speed: 8
	}).run();
	
	new AddOpenMyBurger({
		burger: '.js__navHamburger',
		navbar: '.js__navHamburgerOpener'
	}).run();
	
	new AddMyWorker({
		selector: '.js__myTaskList',
		addbutton: '.js__addNewTask'
	}).run();
	
};

if (typeof window !== 'undefined' && window && window.addEventListener) {
	document.addEventListener('DOMContentLoaded', start(), false);
}
 