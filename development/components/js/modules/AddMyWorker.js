module.exports = class AddOpenMyBurger {
	constructor(init) {
		this.selector = document.querySelector(init.selector);
		this.addbutton = document.querySelector(init.addbutton);
		this.errorText = '💀 Ouch, database error...';

		this.modalInput = document.querySelector('#modal__input_id');
		this.openModal = document.querySelector('.js__openMyModal');
		this.createModal = document.querySelector('.js__myModalCreate');
		this.cardEmpty = document.querySelector('.card__empty');
		this.modalCount = 0;
		this.inputTextLeght = 3;
	}

	static info() {
		console.log('MODULE:', this.name, true);
	}

	run() {
		if (this.selector) {
			this.constructor.info();

			const pastHTML = {
				card: (titleName, pastCount) => (`
									<div class="card__head">
									<h4 class="card__title">${titleName}</h4>
									<div class="card__nav">
									<div class="card__group">
									<div class="card__item card__item-status"><span data-worker-count>${pastCount}</span><p>вакансии</p></div>
									<div class="card__item card__item-add active" data-project>Добавить вакансию</div>
									<div class="card__item card__item-close" data-project>
									<span><svg role="picture-svg" class="glyphs__checkbox"><use xlink:href="#id-glyphs-checkbox"></use></svg></span>
									<p class="card__text">Проект закрыт, сотрудники наняты</p>
									</div>
									</div>
									<div class="card__group">
									<div class="card__item card__item-exit active" data-reopen>Закрыть проект</div>
									<div class="card__item card__item-reopen" data-reopen>Открыть проект</div>
									<div class="card__item">
									<p class="card__text card__text-kill active" data-status>Удалить</p>
									<div class="card__item-remove" data-status>
									<span data-status-yes><svg role="picture-svg" class="glyphs__checkbox"><use xlink:href="#id-glyphs-checkbox"></use></svg></span>
									<span data-status-nope><svg role="picture-svg" class="glyphs__closemodal"><use xlink:href="#id-glyphs-closemodal"></use></svg></span>
									</div>
									</div>
									</div>
									</div>
									</div>
								`),
				cardlist: titleName => (`
									<div class="card-list__head">
									<h4 class="card-list__title active" data-list-title>Пусто</h4>
									<input class="card-list__title card-list__title-edit" type="text" name="text" autocomplete="off" maxlength="120">
									</div>
									<div class="card-list__nav">
									<div class="card-list__group">
									<div class="card-list__item card-list__item-search active" data-list-project>
									<span><svg role="picture-svg" class="glyphs__search"><use xlink:href="#id-glyphs-search"></use></svg></span>
									<p class="card-list__text">Вакансия открыта, идет подбор кандидатов</p>
									</div>
									<div class="card-list__item card-list__item-search" data-list-project>
									<span><svg role="picture-svg" class="glyphs__checkbox"><use xlink:href="#id-glyphs-checkbox"></use></svg></span>
									<p class="card-list__text">Вакансия закрыта, сотрудник нанят</p>
									</div>
									</div>
									<div class="card-list__group js__toggleListReopen">
									<div class="card-list__item card-list__item-exit active" data-list-reopen>Закрыть вакансию</div>
									<div class="card-list__item card-list__item-reopen" data-list-reopen>Открыть вакансию</div>
									<div class="card-list__item js__toggleListKill">
									<p class="card-list__text card-list__text-kill active" data-list-status>Удалить</p>
									<div class="card-list__item-remove" data-list-status>
									<span data-list-status-yes><svg role="picture-svg" class="glyphs__checkbox"><use xlink:href="#id-glyphs-checkbox"></use></svg></span>
									<span data-list-status-nope><svg role="picture-svg" class="glyphs__closemodal"><use xlink:href="#id-glyphs-closemodal"></use></svg></span>
									</div>
									</div>
									</div>
									</div>
								`)
			};

			const observerCardCount = () => {
				if (this.selector.querySelector('.js__toggleStatus') <= 0) {
					this.cardEmpty.classList.add('active');
				} else this.cardEmpty.classList.remove('active');
			};
			const observerCard = new MutationObserver(observerCardCount);
			observerCard.observe(this.selector, { childList: true }, false);

			const updateListCount = (event) => {
				const list = [...document.querySelectorAll('.js__toggleStatus')];
				list.forEach((f) => {
					const count = f.childElementCount - 1;
					const item = f.querySelector('[data-worker-count]');
					item.textContent = count || 0;
				});
			};
			updateListCount();

			const openModalState = (event) => {
				const _target = event.target;
				const animaEnd = () => {
					this.openModal.classList.remove('anima-opacity-hide', 'active');
					this.modalInput.classList.remove('error');
					this.modalInput.value = '';
					this.modalInput.nextElementSibling.textContent = '';
				};
				
				if (_target.matches('.js__openMyModal.active') || _target.matches('.modal__close')) {
					this.openModal.classList.add('anima-opacity-hide');
					this.openModal.addEventListener('animationend', animaEnd, { once: true }, false);
				}
			};
			const addbuttonOpacity = () => {
				const animaEnd = () => this.openModal.classList.remove('anima-opacity-show');
				
				this.openModal.classList.add('active');
				this.openModal.classList.add('anima-opacity-show');
				this.openModal.addEventListener('animationend', animaEnd, { once: true }, false);
			};

			const createNewTask = () => {
				const pastInput = this.modalInput.value || 'Пусто';
				const pastCount = this.modalCount;
				const animaEnd = () => createDiv.classList.remove('anima-add-me');

				const createDiv = document.createElement('DIV');
				createDiv.classList.add('card__box', 'js__toggleStatus', 'anima-add-me');
				createDiv.innerHTML = pastHTML.card(pastInput, pastCount);
				this.selector.appendChild(createDiv);
				this.openModal.classList.toggle('active');
				this.modalInput.value = '';
				createDiv.addEventListener('animationend', animaEnd, { once: true }, false);
			};

			const createNewTaskInputValidat = () => {
				if (this.modalInput.value.length >= this.inputTextLeght) {
					this.modalInput.classList.remove('error');
					this.modalInput.nextElementSibling.textContent = '';
				} 
			};
			
			const createNewTaskValidat = () => {
				if (this.modalInput.value.length <= this.inputTextLeght) {
					this.modalInput.classList.add('error');
					this.modalInput.nextElementSibling.textContent = `Введенные данные меньше ${this.inputTextLeght} символов`;
					return;
				}
				createNewTask();
			};

			const createNewList = (event) => {
				const myTarget = event.target.closest('.js__toggleStatus');

				if (myTarget && event.target.matches('[data-project]')) {
					const count = myTarget.childElementCount || 0;
					const createDiv = document.createElement('DIV');
					createDiv.classList.add('card-list__box');
					createDiv.innerHTML += pastHTML.cardlist('Нажми меня!');

					event.target.closest('.card__box').appendChild(createDiv);
					myTarget.querySelector('[data-worker-count]').textContent = count;
				}
			};

			const toggleActiveClass = (event) => {
				const myTarget = event.target.closest('.js__toggleStatus');
				const myListTargetReopen = event.target.closest('.js__toggleListReopen');
				const myListTargetKill = event.target.closest('.js__toggleListKill');

				if (myTarget && event.target.matches('[data-list-title]')) {
					const _target = event.target;
					const inputText = event.target.nextElementSibling;
					inputText.value = _target.textContent;

					_target.classList.toggle('active');
					_target.nextElementSibling.focus();
					_target.nextElementSibling.select();

					const myFocusFunc = (ev) => { if (ev.key === 'Enter') inputText.blur(); };
					const myBlurOut = () => {
						if (event.defaultPrevented) return;
						_target.textContent = inputText.value || 'Пусто';
						setTimeout(() => _target.classList.toggle('active'), 250);
						inputText.removeEventListener('keypress', myFocusFunc);
					};

					inputText.addEventListener('blur', myBlurOut, { once: true }, false);
					inputText.addEventListener('keypress', myFocusFunc, false);
				}

				if (myTarget && (event.target.matches('[data-status]') || event.target.matches('[data-status-nope]'))) {
					[...myTarget.querySelectorAll('[data-status]')].forEach(f => f.classList.toggle('active'));
				}

				if (myTarget && event.target.matches('[data-status-yes]')) {
					const myCard = event.target.closest('.card__box');
					const animaEnd = () => myCard.remove();
					
					myCard.style.maxHeight = `${myCard.offsetHeight}px`;
					myCard.classList.add('anima-remove-me');
					myCard.addEventListener('animationend', animaEnd, { once: true }, false);
				}

				if (myTarget && event.target.matches('[data-reopen]')) {
					[...myTarget.querySelectorAll('[data-reopen]')].forEach(f => f.classList.toggle('active'));
					[...myTarget.querySelectorAll('[data-project]')].forEach(f => f.classList.toggle('active'));
				}

				if (myListTargetKill && (event.target.matches('[data-list-status]') || event.target.matches('[data-list-status-nope]'))) {
					[...myListTargetKill.querySelectorAll('[data-list-status]')].forEach(f => f.classList.toggle('active'));
				}

				if (myListTargetKill && event.target.matches('[data-list-status-yes]')) {
					const listCount = myTarget.childElementCount - 2 || 0;
					const myCardList = event.target.closest('.card-list__box');
					myCardList.classList.add('anima-remove-me');
					myCardList.addEventListener('animationend', () => myCardList.remove(), { once: true }, false);
					myTarget.querySelector('[data-worker-count]').textContent = listCount;
				}

				if (myListTargetReopen && event.target.matches('[data-list-reopen]')) {
					const myCardList = event.target.closest('.card-list__box');
					[...myListTargetReopen.querySelectorAll('[data-list-reopen]')].forEach(f => f.classList.toggle('active'));
					[...myListTargetReopen.parentElement.querySelectorAll('[data-list-project]')].forEach(f => f.classList.toggle('active'));

					myCardList.querySelector('[data-list-title]').classList.toggle('pointerevents');
				}
			};

			this.addbutton.addEventListener('click', addbuttonOpacity, false);
			this.openModal.addEventListener('click', openModalState, false);
			this.modalInput.addEventListener('input', createNewTaskInputValidat, false);
			this.createModal.addEventListener('click', createNewTaskValidat, false);
			this.selector.addEventListener('click', createNewList, false);
			this.selector.addEventListener('click', toggleActiveClass, false);

		}
	}
};
