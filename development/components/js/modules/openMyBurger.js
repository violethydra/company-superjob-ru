module.exports = class AddOpenMyBurger {
	constructor(init) {
		this.selector = init.burger;
		this.navbar = init.navbar;
		this.errorText = '💀 Ouch, database error...';
	}

	static info() {
		console.log('MODULE:', this.name, true);
	}

	run() {
		const selector = document.querySelector(`${this.selector}`);
		if (selector) {
			this.constructor.info();

			const openBurger = () => {
				const nav = document.querySelector(`${this.navbar}`);
				nav.querySelector('DIV').classList.toggle('open');
				selector.classList.toggle('open');
			};

			selector.addEventListener('click', openBurger);
		}
	}
};
